#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void setMap(char f[]){
	xmlD = xmlParseFile(f);
	xp = xmlXPathNewContext(xmlD);
}

xmlXPathObjectPtr getElement(char *xpath){
	return xmlXPathEvalExpression(BAD_CAST xpath,xp);
}

int comparID(const void * A, const void * B){
	return ((struct geo_point*)A)->id < ((struct geo_point*)B)->id;
}

void init(){
	xmlXPathObjectPtr nodes,bounds,mbuilds;
	
	bounds = getElement("/osm/bounds");
	xmlNodeSetPtr kk = bounds->nodesetval;
	printf("stringval: %s\n",bounds->stringval);
	for(int i=0;i<kk->nodeNr;i++){
		printf("b: %s: %s\n",kk->nodeTab[i]->name,kk->nodeTab[i]->content);
		
		xmlAttr * properties = kk->nodeTab[i]->properties;
		const char * name, * value;
		while(properties!=NULL){
			
			name = (char*)properties->name;
			value = (char*)xmlNodeListGetString(properties->doc, properties->children, 1);
			printf("attr: %s %s\n",name,value);
			if(!strcmp(name,"minlat")){
				sscanf(value,"%Lf",&minlat);
			}else if(!strcmp(name,"minlon")){
				sscanf(value,"%Lf",&minlon);
			}else if(!strcmp(name,"maxlat")){
				sscanf(value,"%Lf",&maxlat);
			}else if(!strcmp(name,"maxlon")){
				sscanf(value,"%Lf",&maxlon);
			}else{
				fprintf(stderr,"Error: problem with <bounds> %s\n",name);
			}

			properties = properties->next;
		}
		printf("%Lf %Lf %Lf %Lf\n",minlat,minlon,maxlat,maxlon);
	}
	xmlFree(bounds);


	nodes = getElement("/osm/node");
//	way = getElement("/osm/way/tag[@k = \"highway\"]/..");
	mbuilds = getElement("/osm/way/tag[@k = \"building\" and @v = \"yes\"]/..");
	
	
	kk = nodes->nodesetval;
	
	geo_nodes_length = kk->nodeNr;

	geo_nodes = malloc(sizeof(struct geo_point) * geo_nodes_length);

	int id;
	long double lat,lon;
	
	for(size_t i=0;i< geo_nodes_length ;i++){
	//	printf("b: %s: %s\n",kk->nodeTab[i]->name,kk->nodeTab[i]->content);
		
		xmlAttr * properties = kk->nodeTab[i]->properties;
		const char * name, * value;
		while(properties!=NULL){
		
			name = (char*)properties->name;
			value = (char*)xmlNodeListGetString(properties->doc, properties->children, 1);
		//	printf("attr: %s %s\n",name,value);
		
			if(!strcmp(name,"id")){
				sscanf(value,"%d",&id);
			}else if(!strcmp(name,"lat")){
			sscanf(value,"%Lf",&lat);
			}else if(!strcmp(name,"lon")){
				sscanf(value,"%Lf",&lon);
			}
					properties = properties->next;
		}
		geo_nodes[i] = (struct geo_point){id,lat,lon};
	}
	xmlFree(nodes);

	kk = mbuilds->nodesetval;
	
	mbuildsID_length = kk->nodeNr;

	mbuildsID = malloc(sizeof(int)*mbuildsID_length);

	for(size_t i=0;i<mbuildsID_length;i++){
		sscanf((char*)xmlXPathCastNodeSetToString(kk),"%d",&mbuildsID[i]);
	}
	xmlFree(mbuilds);

	qsort(mbuildsID,mbuildsID_length,sizeof(struct geo_point),comparID);
}

int Mapermain(){
	
	setMap("map.osm");
	
	init();

	return 0;
	/*
	xmlXPathObjectPtr ob = getElement(argc[1]);
	
	if(ob!=NULL){
		
		for(int i=0;i<ob->nodesetval->nodeNr;i++){
			printf("K[%d]:\n%s\n",i,xmlXPathCastNodeToString(*(ob->nodesetval->nodeTab+i)));
		}
	}else
		printf("nie znaleziono\n");
*/
}
