/** hexagon
 *
 * @author Samuel Li (307724)
 */

#include <stdlib.h>
#include <stdio.h>

#ifndef __HEXAGONAL_STRUCTURE_
#define __HEXAGONAL_STRUCTURE_

struct hexagon {
	void * element;
	struct hexagon	* upleft,
					* upright,
					* left,
					* right,
					* downleft,
					* downright;
};

enum side {upleft,upright,left,right,downleft,downright};

typedef struct hexagon	hexagon;

hexagon ** getSide(hexagon * reference, enum side s){
	switch(s){
	case upleft:
		return &reference->upleft;
	case upright:
		return &reference->upright;
	case left:
		return &reference->left;
	case right:
		return &reference->right;
	case downleft:
		return &reference->downleft;
	case downright:
		return &reference->downright;
	}
	return NULL;
}

hexagon ** nextSide(hexagon * reference, enum side s){
	switch(s){
	case upleft:
		return &reference->downright;
	case upright:
		return &reference->downleft;
	case left:
		return &reference->right;
	case right:
		return &reference->left;
	case downleft:
		return &reference->upright;
	case downright:
		return &reference->upleft;
	}
	return NULL;
}


hexagon * newhexagon(){
	return calloc(1,sizeof(hexagon));
}

hexagon * create(void * e){
	hexagon * h = calloc(1,sizeof(hexagon));
	h->element = e;
	return h;
}

void connecthex(hexagon * a, hexagon * b, enum side s){
	*getSide(a,s) = b;
	*nextSide(b,s) = a;
}

void unconnecthex(hexagon * a, hexagon * b, enum side s){
	*getSide(a,s) = NULL;
	*nextSide(b,s) = NULL;
}

#define forsides(k,doing) ({\
	for(int k=upleft;i<=downright;i++)\
		doing\
})

void snatchhex(hexagon * a){
	hexagon ** r;
	for(int s=upleft;s<=downright;s++){
		r = getSide(a,s);
		if(*r != NULL){
			unconnecthex(a,*r,s);
		}
	}
}

hexagon * createAt(hexagon * reference, enum side s){
	hexagon * neighbor = newhexagon();
	connecthex(reference,neighbor,s);
	return neighbor;
}

hexagon * addHexagon(hexagon * reference, enum side s, void * e){
	hexagon * neighbor = create(e);
	connecthex(reference,neighbor,s);
	return neighbor;
}

void setHexagon(hexagon * reference, enum side s, void * e){
	(*getSide(reference,s))->element = e;
}

void setElement(hexagon * reference, void * e){
	reference->element = e;
}

void put(hexagon * h, void * e){
	h->element = e;
}

#define hexput(h,e) ({\
	void * r = malloc(sizeof(e));	\
	*((__typeof__ (e) *)r) = (e);							\
	put((h),r);						\
})

void * get(hexagon * h){
	return h->element;
}

void unnewhexagon(hexagon * h){
	snatchhex(h);
	free(h);
}

#define hexget(h,a)	a = get(h)

#ifdef __DEMO__

#include <stdio.h>
#include <string.h>

#define metyp double
#define sttyp "%lf"

#define width 100
#define height 10

#define hexwidth 11

char wyp[height][width];

char unzero(char a){
	if(a==0)
		return ' ';
	return a;
}

hexagon ** getSideS(hexagon * reference, char * c){
	if(!strcmp("upleft",c))
		return &reference->upleft;
	else if(!strcmp("upright",c))
		return &reference->upright;
	else if(!strcmp("left",c))
		return &reference->left;
	else if(!strcmp("right",c))
		return &reference->right;
	else if(!strcmp("downleft",c))
		return &reference->downleft;
	else if(!strcmp("downright",c))
		return &reference->downright;
	else
		return NULL;
}

enum side getSideEnum(char * c){
	if(!strcmp("upleft",c))
		return upleft;
	else if(!strcmp("upright",c))
		return upright;
	else if(!strcmp("left",c))
		return left;
	else if(!strcmp("right",c))
		return right;
	else if(!strcmp("downleft",c))
		return downleft;
	else if(!strcmp("downright",c))
		return downright;
	else
		return 255;
}

void wypisz(){
	for(int y=0;y<height;y++){
		for(int x=0;x<width;x++){
			printf("%c",unzero(wyp[y][x]));
		}
		printf("\n");
	}
}

void clear(){
	for(int y=0;y<height;y++){
		for(int x=0;x<width;x++){
			wyp[y][x]=' ';
		}
	}
}

void draw(int x, int y,int w){
	//wyp[y][x]='@';
	wyp[y][x+1]='/';
	wyp[y+1][x]='|';
	wyp[y+2][x+1]='\\';
	
	wyp[y][x+w-1]='\\';
	wyp[y+1][x+w]='|';
	wyp[y+2][x+w-1]='/';
}

void drawT(int x, int y, int w, hexagon * h){
//	printf("add: %p\n",h);
	if(h!=NULL){
		draw(x,y,w);
		if(h->element!=NULL)
			sprintf(&wyp[y+1][x+1],sttyp,*(metyp*)h->element);
	}
}

void plan(hexagon * h){
	clear();
	drawT(2*hexwidth,4,hexwidth,h);
	drawT(0,4,hexwidth,h->left);
	drawT(hexwidth,0,hexwidth,h->upleft);
	drawT(3*hexwidth,0,hexwidth,h->upright);
	drawT(4*hexwidth,4,hexwidth,h->right);
	drawT(hexwidth,7,hexwidth,h->downleft);
	drawT(3*hexwidth,7,hexwidth,h->downright);
	wypisz();
}

int main(){
	char w[100];
	int k;
	metyp ll;
	hexagon * h = newhexagon();
	while(1){
		plan(h);
		printf("> ");
		//fgets(w,100,stdin);
		if((k=scanf("%s",w))==1){
			printf("commend: \"%s\"\n",w);
			if(!strcmp("help",w)){
				printf("set,get\n");
			}else if(!strcmp("set",w)){
				scanf(sttyp,&ll);
				hexput(h,ll);
			}else if(!strcmp("get",w)){
				printf("element:");
				printf(sttyp,*((metyp*)(h->element)));
				printf("\n");
			}else if(!strcmp("print",w)){
				plan(h);
			}else if(!strcmp("exit",w)){
				return 0;
			}else if(!strcmp("goto",w)){
				scanf("%s",w);
				hexagon ** rr = getSideS(h,w);
				if(rr!=NULL){
					if(*rr != NULL){
						h = *rr;
					}else
						printf("Przepaść\n");
				}else{
					printf("Bad side\n");
				}
			}else if(!strcmp("new",w)){
				scanf("%s",w);
				enum side rr = getSideEnum(w);
				hexagon * hh = newhexagon();
				if(rr != 255){
					if(*getSide(h,rr)==NULL){
						connecthex(h,hh,rr);
					}else
						printf("Zajente\n");
				}else{
					printf("Bad side\n");
				}

			}else{
				printf("Error: Uncnown command!\n");
			}
		}else{
			printf("Error: bad input\n");
		}
		if(k<0){
			return 0;
		}
	}

}

#endif

#endif
