#include <complex.h>
#include "euklides.h"

#include "utlist.h"

#define MaxN 100


#define car_size 1
#define build_radius 3


#define csgn(c) cexp(carg(c)*I)

struct build{
	point X;
	size_t id,nid;
	struct build * next;
};

struct car{
	double v,a;
	double r,g,b;
	point X;
	void (* AI)(struct car *);
	struct build * target;
	size_t id;
};

typedef struct el_build{
	struct build b;
	struct el_build *next, *prev;
} el_build;

typedef struct el_car{
	struct car b;
	struct el_car *next, *prev;
} el_car;

extern gboolean thread_lock;
extern el_build * builds;
extern el_car * cars;
extern gboolean run_symulation;

void readCars(FILE *);
void saveCars(FILE *);

gboolean euler(gpointer);

point getHead(const struct car);
point getBack(const struct car);
point getLeft(const struct car);
point getRight(const struct car);

void willcolision(const struct car,int ,vector [],int[]);

void clean_all();
void addnewbuild(struct build);
void addnewcar(struct car);

void AI1(struct car *);
