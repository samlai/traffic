#include <libxml/encoding.h>
#include <libxml/xpath.h>
#include <libxml/xmlwriter.h>
#include <libxml/list.h>


long double minlat, minlon, maxlat, maxlon;

struct geo_point{
	int id;
	long double lat,lon;
};

size_t geo_nodes_length;

struct geo_point * geo_nodes;

size_t mbuildsID_length;
int * mbuildsID;

xmlDocPtr xmlD;
xmlXPathContextPtr xp;

//xmlXPathObjectPtr way;

void init();
int comparID(const void * A, const void * B);
xmlXPathObjectPtr getElement(char *xpath);
void setMap(char f[]);

int Mapermain();
