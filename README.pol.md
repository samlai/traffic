# Traffic Simulator
 
Symulator Korków ulicznych.

## Wstęp

Projekt stworzony w ramach zajęci Uniwersytetu Wrocławskiego kurs ANSI C.

Program ma za zadanie pośrednio symulować korki uliczne, poprzez symulacje pojedynczych samochodów.

## Opis Zastosowania

Symulacja korków ulicznych pozwoli na optymalizacje ulic w miastach bądź tras w fabrykach.

 - [x] Dodawanie budynków
 - [x] Usuwanie budynków
 - [x] Dodawanie samochodów
 - [x] Usuwanie samochodów
 - [x] Ustawianie celów odsyłania na budynkach
 - [x] Zatrzymanie symulacji
 - [x] Przybliżanie i oddalanie mapy
 - [x] Zapis i odczyt mapy z plików (format opisany poniżej)
 - [ ] Wczytywanie plików `xml` z [OpenStreetMap]

## Opis Implementacji

Do wyświetlania w okienku wykorzystywana jest załączona w [GTK3+] biblioteka.

Program wykorzystuje do symulacji metodę [Euler]a uruchomioną w osobnym wątku.

### Moduły

##### Plik `drawing.h`
Funkcjonalność, które odpowiada za rysowanie okienka oraz za grafikę.

##### Plik `maper.h`
Wczytywanie map z plików `xml` pochodzących z [OpenStreetMap].
Obecnie nie używany!!!

##### Plik `car.h`
Symulacja kolejnych kroków symulacyjnych metodą Eulera.
Elementy sztucznej inteligencji.

##### Plik `euklides.h`
Funkcje związane z obliczeniami matematycznymi w przestrzeni euklidesowej.
Implementacja struktur matematycznych.

## Wymagania

* Działający kompilator ANSI C11
* Zainstalowana biblioteka [GTK3+]

### Kompilacja

W celu kompilacji generujemy najpierw plik `Makefile` przez uruchomienie skryptu `configure` po czym rozpoczynamy kompilacje przez `make`:

```console
$ ./configure
$ make
```

Jeżeli kompilacja przebiegła pomyślnie to powstanie skompilowany plik `traffic`.

### Uruchomienie

Program uruchamiamy z konsoli poprzez wywołanie `traffic`.

```console
$ ./traffic
```

Po tym powinno pojawić się okno programu.

Jeżeli chcesz od razu uruchomić program z zapisanym stanem miasta, podajesz go w pierwszym argumencie.
Mamy dwa pliki testowe `city.tra` oraz `manhattan.tra`

```console
$ ./traffic city.tra
```

```console
$ ./traffic manhattan.tra
```

## Użytkowanie

By wykonać interakcję z mapą klikamy w wybranym miejscu lewym przyciskiem myszy, zaś by ją przesunąć prawym przyciskiem myszy.

### Opcje

|   Checkbox    |                      Opis                     |
|:------------- |:--------------------------------------------- |
| ☑ see road    | Ustawia widoczność dróg.                      |
| ☑ see target  | Pokazuje cele podróży każdego z samochodów.   |
| ☑ run         | Uruchamia symulację.                          |

### Powiększanie i przybliżanie

|Przycisk|             Opis              |
|:------:|:----------------------------- |
|![zoom+]| Powiększanie obrazu.          |
|![zoom-]| Pomniejszanie obrazu.         |
|![zoom0]| Ustawienie normalnego widoku. |

### Interakcja z mapą

|   Combo box   |               Opis                |
|:-------------:|:--------------------------------- |
|![build_Add]   | Tworzenie budynku.                |
|![build_rem]   | Usuwanie budynku.                 |
|![car_add]     | Dodanie samochodu.                |
|![car_rem]     | Usuwanie samochodu.               |
|![build_target]| Ustawienie następnego celu.       |

### Ustawianie następnego celu

By ustawić następny cel klikamy na budynek, do którego ma mają zmierzać samochody.
Wówczas powinno wokół niego pojawić się niebieska poświata.
Następnie klikamy na budynek, od którego mają zaczynać podróż.

Jeżeli jest włączona opcja:
 ☑ see road
powinieneś ujrzeć nowo stworzoną drogę pomiędzy tymi budynkami.

## Opis plików z mapami

Plik składa się z opisu kolejnych budynków i samochodów.
Każda linia opisuje jeden element - albo budynek albo pojazd.

W każdej linijce znajduje się na początku nawias kwadratowy, a w nim litera mówiąca czego jest opisem.
`[b]` oznacza opis budynku zaś `[c]` opis pojazdu.

### budynek

Po zasygnalizowaniu, że linia opisuje budynek zostają podane dwie liczby zmiennoprzecinkowe, które mówią o współrzędnych, po czym dwie liczby naturalne opisujące ID budynku i ID budynku do którego mają się skierować samochody po odwiedzeniu tego budynku.

```
[b] 134.000000 286.000000 22615424 25305920
```

Linijka mówi o budynku w punkcie $\(134.0, 286.0\)$, którego ID to `22615424`, a pojazdy po odwiedzeniu tego budynku mają się kierować do budynku o ID `25305920`.

Drogi są tworzone na podstawie informacji o budynkach.

### pojazd

Po zasygnalizowaniu, że linia opisuje pojazd zostają podane cztery liczby zmiennoprzecinkowe, które mówią o współrzędnych, oraz prędkości pojazdu oraz kącie (w radianach) pod jakim się znajduje względem poziomu mapy.

```
[c] 450.495382 284.466504 1.500000 3.136747
```

Linijka mówi o samochodzie w punkcie $\(450.495382, 284.466504\)$ pędzącego z prędkością $1.5$ jednostek i ustawionego pod kątem $3.13$ tzn. mniej więcej ustawionego w lewo.

Pojazd kieruje się w stronę ostatnio podanego budynku.

## Referencje

* GTK+ 3 Reference Manual: https://developer.gnome.org/gtk3

[GTK3+]: https://developer.gnome.org/gtk3/

[OpenStreetMap]: https://www.openstreetmap.org
[Euler]: https://pl.wikipedia.org/wiki/Metoda_Eulera

[zoom+]: https://dabuttonfactory.com/button.png?t=zoom%2B&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[zoom-]: https://dabuttonfactory.com/button.png?t=zoom-&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[zoom0]: https://dabuttonfactory.com/button.png?t=zoom0&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[build_add]: https://dabuttonfactory.com/button.png?t=Add+build&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[build_rem]: https://dabuttonfactory.com/button.png?t=Remove+build&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[car_add]: https://dabuttonfactory.com/button.png?t=Add+car&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[car_rem]: https://dabuttonfactory.com/button.png?t=Remove+car&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[build_target]: https://dabuttonfactory.com/button.png?t=Build+target&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000

