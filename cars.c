#include <stdio.h>
#include <gtk/gtk.h>
#include <math.h>
#include <stdlib.h>
#include <locale.h>

#include "cars.h"
#include "euklides.h"

long double dt = 0.05, dalf=G_PI/(360);

gboolean thread_lock = TRUE;

el_build * builds = NULL;
el_car * cars = NULL;

gboolean run_symulation = TRUE;

#define epsilon 0.0

#define distance(a,b) cabs((b)-(a))

point getHead(const struct car c){
	return cexp(I*c.a) * 5*car_size + c.X;
}

point getBack(const struct car c){
	return c.X - cexp(c.a*I) * car_size;
}

point getLeft(const struct car c){
	return car_size*3 * cexp((3.0*G_PI/4.0 + c.a)*I) + c.X;
}

point getRight(const struct car c){
	return car_size*3 * cexp((-3.0*G_PI/4.0  + c.a)*I) + c.X;
}

struct triangle getCoachbuilder(const struct car c){
	return (struct triangle){getHead(c),getLeft(c),getRight(c)};
}

void willcolision(const struct car a, int n, vector tt[], int out[]){
	el_car *e_car;
	DL_FOREACH(cars,e_car)
		for(int i=0;i<n;i++)
			if(	e_car->b.id != a.id &&
				przetnie_w_przyszlosci(
					getCoachbuilder(a), tt[i]*a.v*cexp(a.a*I),
					getCoachbuilder(e_car->b), a.v*cexp(a.a*I)))
				out[i]=1;
			else
				out[i]=0;
}

double przedemnom(const struct car * c){
	el_car *ec;
	point K = c->X + 5*cexp(c->a*I);
	double dis=1000;
	LL_FOREACH(cars,ec){
		if(&(ec->b) != c)
			dis = fmin(dis,distance(K,ec->b.X));
	}
	return dis;
}
/*
static double norm_angle(double a){
	while(a > G_PI){
		a -= 2*G_PI;
	}
	while(a < -G_PI){
		a += 2*G_PI;
	}
	return a;
}*/
/*
static double slowAngle(double a){
	if(a>epsilon)
		return a;
	else if(a<-epsilon)
		return -a;
	else
		return 0;
}*/
/*
void AI1(struct car * c){
	double da;
	if(c->target == NULL){
		c->v = 0.1;
		return;
	}
	vector at = (c->target->X) - (c->X);
	if(cabs(at) < 20){
		c->target = c->target->next;
	}else{
		da = norm_angle(carg(at) - c->a);
		int r[3] = {0,0,0};
		willcolision(*c,3,(vector[]){20,20+20*I,20-20*I},r);
		if(r[1]&r[2]){
		//	printf("P\n");
			c->v = -1;
		}else if(r[0]){
		//	printf("C\n");
			c->v = -1.0 + round(1.0);
		}else if(r[1]){
		//	printf("L\n");
			c->a += da/1000 - G_PI/2048.0;
			c->v = 0.2;
		}else if(r[2]){
		//	printf("R\n");
			c->a += da/1000 + G_PI/2048.0;
			c->v = 0.2;
		}else{
			c->a += da/1000;
			c->v = 1;
		}
	}
}
*/
void AI2(struct car * c){
	double dis;
	if(c->target == NULL){
		if(builds!=NULL)
			c->target = &(builds->b);
		c->v = 0;
		return;
	}
	vector at = (c->target->X) - (c->X);
	if(cabs(at) < 1){
		c->target = c->target->next;
	}else{
		//da = norm_angle(carg(at) - c->a);
		c->a = carg(at);
		c->r = (przedemnom(c)<5)?1:0.5;
		//c->b = (abs(da) == 0)?0.5:1;
		dis = przedemnom(c);
		if(dis < 4)
			c->v = 0;
		else if(dis < 5)
			c->v = 0.5;
		else if(dis > 10)
			c->v = 1.5;
		else
			c->v = 1;
	}

}

gboolean euler(gpointer T){
	if(run_symulation){
		el_car *e_car;
		DL_FOREACH(cars,e_car){
			e_car->b.AI(&e_car->b);
			e_car->b.X = e_car->b.X + e_car->b.v*cexp(I*e_car->b.a) * dt;
		}
	}
	return TRUE;
}

void addnewbuild(struct build b){
	thread_lock = FALSE;
	el_build * pb = malloc(sizeof(el_build));
	pb->b = b;
	pb->b.next = &pb->b;
	DL_APPEND(builds,pb);
	thread_lock = TRUE;
}

void addnewcar(struct car c){
	thread_lock = FALSE;
	el_car * el = calloc(1,sizeof(struct el_car));
	el->b = c;
	el->b.AI = AI2;
	el->b.target = &builds->b;
	el->b.r = 0.5;
	el->b.g = rand()*1.0/RAND_MAX;
	el->b.b = rand()*1.0/RAND_MAX;
	DL_APPEND(cars,el);
	thread_lock = TRUE;
}
void clean_all(){
	el_build * b;
	LL_FOREACH(builds,b){
		LL_DELETE(builds,b);
	}
	el_car * c;
	LL_FOREACH(cars,c)
		LL_DELETE(cars,c);
}
void readCars(FILE * f){
	printf("Set locale: %s\n",setlocale(LC_NUMERIC, "C"));
	el_car * m_car;
	el_build * m_build,*p_build;
	int k;
	char c;
	double x,y,v,a;
	long int id,nid;
	while((k=fscanf(f,"[%c] %lf %lf",&c,&x,&y))==3){
		if(c=='b'){
			fscanf(f,"%ld %ld\n",&id,&nid);
			m_build = malloc(sizeof(el_build));
			m_build->b.X = x + y*I;
			m_build->b.id = id;
			m_build->b.nid = nid;
			m_build->b.next = NULL;
			DL_APPEND(builds,m_build);
		}else if(c=='c'){
			fscanf(f,"%lf %lf\n",&v,&a);
			m_car = malloc(sizeof(el_car));
			m_car->b.X = x + y*I;
			m_car->b.v = v;
			m_car->b.a = a;
			m_car->b.AI = AI2;
			m_car->b.r = 0.5;
			m_car->b.g = rand()*1.0/RAND_MAX;
			m_car->b.b = rand()*1.0/RAND_MAX;
			m_car->b.target = (m_build==NULL)?NULL:&m_build->b;
			DL_APPEND(cars,m_car);
		}else{
			fprintf(stderr,"Error: Problem witch reading file!\n");
			fscanf(f,"%*[^\n]\n");
		}
	}
	printf("File loaded!\n");
	LL_FOREACH(builds,m_build){
		LL_FOREACH(builds,p_build){
			if(m_build->b.nid==p_build->b.id){
				m_build->b.next = &p_build->b;
				break;
			}
		}
	}
}
void saveCars(FILE * f){
	printf("Set lovale: %s\n",setlocale(LC_NUMERIC, "C"));
	el_car * c;
	el_build * b;
	LL_FOREACH(cars,c){
		if(c->b.target == NULL){
			fprintf(f,"[c] %f %f %f %f\n",
				creal(c->b.X),
				cimag(c->b.X),
				c->b.v,
				c->b.a
			);
			printf("[c] %f %f %f %f\n",
				creal(c->b.X),
				cimag(c->b.X),
				c->b.v,
				c->b.a
			);

		}
	}
	LL_FOREACH(builds,b){
		fprintf(f,"[b] %f %f %ld %ld\n",
			creal(b->b.X),
			cimag(b->b.X),
			(long int)&(b->b),
			(long int)b->b.next);
		LL_FOREACH(cars,c){
			if(c->b.target == &(b->b)){
				fprintf(f,"[c] %f %f %f %f\n",
					creal(c->b.X),
					cimag(c->b.X),
					c->b.v,
					c->b.a
				);
			}
		}
	}
}
