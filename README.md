# Traffic Simulator
 
Traffic jam simulator.

## Introduction

Project created as part of [the University of Wroclaw] ANSI C classes.

## Description

Simulation of traffic jams will allow you to optimize streets in cities or routes in factories.

 - [x] Adding buildings
 - [x] Deleting buildings
 - [x] Adding cars
 - [x] Deleting cars
 - [x] Setting posting targets on buildings
 - [x] Stop the simulation
 - [x] Zooming in and out of the map
 - [x] Saving and opening the map form files (format described below)
 - [ ] Loading `xml` files from [OpenStreetMap]

## Implementation description

For display is used the [GTK3+] library.

The program uses the [Euler] method.

### Modules

##### File `drawing.h`
It is responsible for drawing the window and graphics.

##### File `maper.h`
Loading maps from `xml` files from [OpenStreetMap].
Currently not used!!!

##### File `car.h`
Simulation of subsequent simulation steps using the [Euler] method.
Elements of artificial intelligence.

##### File `euklides.h`
Mathematical calculations in Euclidean space.
Implementation of mathematical structures.

## Requirements

* ANSI C11 compiler
* GTK3+ library

### Compilation

To compile, first, generate the `Makefile` file by running the `./configure` script and then start the compilation with `make`:

```console
$ ./configure
$ make
```

After the compilation, we get a `traffic` file.

### Running the program

Once it is built, you can run the `traffic` executable after you enter your traffic directory and run:

```console
$ ./traffic
```

After that, the program window should appear.

If you want to start the program immediately with the saved state of the city, enter it in the first argument.
We have two test files `city.tra` and `manhattan.tra`:

```console
$ ./traffic city.tra
```

```console
$ ./traffic manhattan.tra
```

## Use

To interact with the map, click the left mouse button in the selected place, and to move it with the right mouse button.

### Options

|   Checkbox    |                  Description                  |
|:------------- |:--------------------------------------------- |
| ☑ see road    | It sets the visibility of the roads.          |
| ☑ see target  | It shows the destinations of each car.        |
| ☑ run         | It runs the simulation.                       |

### Zooming

| Button | Description |
|:------:|:----------- |
|![zoom+]| Zoom in     |
|![zoom-]| Zoom out    |
|![zoom0]| Zoom Reset  |

### Interacting with the map

|   Combo box   |           Description             |
|:-------------:|:--------------------------------- |
|![build_Add]   | Creating buildings.               |
|![build_rem]   | Deleting buildings.               |
|![car_add]     | Creating cars.                    |
|![car_rem]     | Deleting cars.                    |
|![build_target]| Setting the next destination.     |

### Setting the next destination

To set the next goal, click on the building where the cars are to go.
Then a blue glow should appear around him.
Then click on the building from which to start the journey.

If the option is enabled:
 ☑ see road
you should see the newly created path between these buildings.

## Description of map files

The file consists of a description of subsequent buildings and cars.
Each line describes one element - either a building or a vehicle.

At the beginning of each line, there is a square bracket with a letter saying, what is the description.
`[b]` means the building description and `[c]` vehicle description.

### Building

If the line describes the building, we get two floating-point numbers, which speak of coordinates, followed by two natural numbers the ID of this build and the ID of build to which the cars are to go after visiting this building.

```
[b] 134.000000 286.000000 22615424 25305920
```

The line says about the building at the point $\(134.0, 286.0 \)$, whose ID is `22615424`, and the vehicles after visiting this building are to go to the building with the ID `25305920`.

Roads are created based on information about buildings.

### Vehicles

If the line describes the vehicle, we get four floating-point numbers, which tell about the coordinates, the vehicle speed and angle (in radians).

```
[c] 450.495382 284.466504 1.500000 3.136747
```

The line says about the car at $\(450.495382, 284.466504 \)$ with a speed of $ 1.5 $ units and with angle $3.13$.

## Reference

* GTK+ 3 Reference Manual: https://developer.gnome.org/gtk3
* Reference Manual for libxml2: http://xmlsoft.org/html/index.html

[the University of Wroclaw]: https://uni.wroc.pl
[OpenStreetMap]: https://www.openstreetmap.org
[Euler]: https://en.wikipedia.org/wiki/Euler_method

[zoom+]: https://dabuttonfactory.com/button.png?t=zoom%2B&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[zoom-]: https://dabuttonfactory.com/button.png?t=zoom-&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[zoom0]: https://dabuttonfactory.com/button.png?t=zoom0&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[build_add]: https://dabuttonfactory.com/button.png?t=Add+build&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[build_rem]: https://dabuttonfactory.com/button.png?t=Remove+build&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[car_add]: https://dabuttonfactory.com/button.png?t=Add+car&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[car_rem]: https://dabuttonfactory.com/button.png?t=Remove+car&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000
[build_target]: https://dabuttonfactory.com/button.png?t=Build+target&f=Ubuntu&ts=15&tc=000&w=110&h=40&c=5&bgt=unicolored&bgc=f3f3f3&be=1&bs=1&bc=000

