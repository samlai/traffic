#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <complex.h>
#include <string.h>
#include <libgen.h>

#include "cars.h"

#ifdef __MAPER__
#include "maper.h"
#endif
/* Surface to store current scribbles */
static cairo_surface_t *surface = NULL;
GtkBuilder *builder;
GtkWidget *window;
GtkWidget *frame;
GtkWidget *drawing_area;

gboolean pressed = 0;
static int 	pressed_x = 0,
			pressed_y = 0,
			translation_x = 0,
			translation_y = 0,
			translation_yd = 0,
			translation_xd = 0;

gboolean see_road = TRUE,see_target = FALSE;

const char * unname_filename = "unnamed.tra";
char * filename = NULL;
#define title_format "[%s] traffic symulator"

int mouse_action_id = 0;

#define BUILD_ADD_ID 0
#define BUILD_REM_ID 1
#define CAR_ADD_ID 2
#define CAR_REM_ID 3
#define BUILD_TARGET 4

struct build * select_build = NULL;

static double zoom=2.0;

static void zoom_plus(GtkWidget *widget, GdkEventConfigure *event, gpointer data){
	zoom*=2;
}

static void zoom_minus(GtkWidget *widget, GdkEventConfigure *event, gpointer data){
	zoom/=2.0;
}

static void zoom_zero(GtkWidget *widget, GdkEventConfigure *event, gpointer data){
	zoom=2.0;
}
static void set_title_file(char * fn){
	char * title = malloc((strlen(fn)+strlen(title_format))*sizeof(char));
	sprintf(title,title_format,fn);
	gtk_window_set_title (GTK_WINDOW (window), title);
}
static void readFile(char * fn){
//	if(filename != unname_filename && filename != NULL)
//		g_free(filename);
	if(fn == NULL){
		filename = (char*)unname_filename;
	}else{
		filename = fn;
		printf("Open file: %s\n",fn);
		FILE * f = fopen(fn,"r");
		readCars(f);
		printf("End read\n");
		fclose(f);
	}
	set_title_file(filename);
}
static void file_open(GtkWidget *widget, GdkEventConfigure *event, gpointer data){
	printf("Open file\n");

	GtkWidget *dialog;
	GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
	gint res;

	dialog = gtk_file_chooser_dialog_new("Open File",GTK_WINDOW(window),action,
		"_Cancel",GTK_RESPONSE_CANCEL,"_Open",GTK_RESPONSE_ACCEPT,NULL);

	res = gtk_dialog_run (GTK_DIALOG (dialog));
	if (res == GTK_RESPONSE_ACCEPT){
		char *fn;
		GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
		fn = gtk_file_chooser_get_filename (chooser);
		clean_all();
		readFile(fn);
	//	g_free(fn);
	}

	gtk_widget_destroy (dialog);
}

static void file_save(GtkWidget *widget, GdkEventConfigure *event, gpointer data){
	printf("Open save\n");
	GtkWidget *dialog;
	GtkFileChooser *chooser;
	GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
	gint res;

	dialog = gtk_file_chooser_dialog_new ("Save File",GTK_WINDOW(window),action,
		"_Cancel",GTK_RESPONSE_CANCEL,"_Save",GTK_RESPONSE_ACCEPT,NULL);

	chooser = GTK_FILE_CHOOSER (dialog);

	gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

//	if (user_edited_a_new_document)
		gtk_file_chooser_set_current_name (chooser,basename(filename));
//	else
//		gtk_file_chooser_set_filename (chooser,existing_filename);

	res = gtk_dialog_run (GTK_DIALOG (dialog));
	if (res == GTK_RESPONSE_ACCEPT){
		char *fn;

		fn = gtk_file_chooser_get_filename (chooser);
		set_title_file(fn);
		FILE * f = fopen(fn,"w");
		saveCars(f);
		fclose(f);
		g_free (fn);
	}

	gtk_widget_destroy (dialog);
}

static int tx(int x){
	return x*zoom + translation_x + translation_xd;
}

static int ty(int y){
	return y*zoom + translation_y + translation_yd;
}

static int ctx(int mx){
	return ( mx - translation_xd - translation_x ) / zoom;
}

static int cty(int my){
	return ( my - translation_yd - translation_y ) / zoom;
}

static void clear_surface (void){
	cairo_t *cr;

	cr = cairo_create (surface);

	cairo_set_source_rgb (cr, 1, 1, 1);
	cairo_paint (cr);

	cairo_destroy (cr);
}

/* Create a new surface of the appropriate size to store our scribbles */
static gboolean configure_event_cb (GtkWidget *widget, GdkEventConfigure *event, gpointer data){
//	printf("configure_event_cb\n");
	if (surface)
		cairo_surface_destroy (surface);

	surface = gdk_window_create_similar_surface (gtk_widget_get_window (widget),
			CAIRO_CONTENT_COLOR,
			gtk_widget_get_allocated_width (widget),
			gtk_widget_get_allocated_height (widget));

	/* Initialize the surface to white */
	clear_surface ();
	
	/* We've handled the configure event, no need for further processing. */
	return TRUE;
}

/* Redraw the screen from the surface. Note that the ::draw
 * signal receives a ready-to-be-used cairo_t that is already
 * clipped to only draw the exposed areas of the widget
 */
static gboolean draw_cb (GtkWidget *widget, cairo_t *cr, gpointer data){
//	printf("draw_cb\n");
	cairo_set_source_surface (cr, surface, 0, 0);
	cairo_paint (cr);

	return FALSE;
}

static void draw_car(cairo_t *cr, const struct car c){
	cairo_set_source_rgb(cr,c.r,c.g,c.b);
	point 	A = getHead(c),
			B = getBack(c),
			L = getLeft(c),
			R = getRight(c);
	cairo_move_to(cr,tx(creall(A)),ty(cimagl(A)));
	cairo_line_to(cr,tx(creall(L)),ty(cimagl(L)));
	cairo_line_to(cr,tx(creall(B)),ty(cimagl(B)));
	cairo_line_to(cr,tx(creall(R)),ty(cimagl(R)));
	
	cairo_fill (cr);
}

static void draw_road(cairo_t *cr, point M, point K){
	cairo_move_to(cr,tx(creal(K)),ty(cimagl(K)));
	if(creall(K)>creal(M)){
		if(cimagl(K)>cimagl(M)){
			cairo_line_to(cr,tx(creal(M)-build_radius),ty(cimagl(M)+build_radius));
			cairo_line_to(cr,tx(creal(M)+build_radius),ty(cimagl(M)-build_radius));
		}else{
			cairo_line_to(cr,tx(creal(M)-build_radius),ty(cimagl(M)-build_radius));
			cairo_line_to(cr,tx(creal(M)+build_radius),ty(cimagl(M)+build_radius));
		}
	}else{
		if(cimagl(K)>cimagl(M)){
			cairo_line_to(cr,tx(creal(M)+build_radius),ty(cimagl(M)+build_radius));
			cairo_line_to(cr,tx(creal(M)-build_radius),ty(cimagl(M)-build_radius));
		}else{
			cairo_line_to(cr,tx(creal(M)+build_radius),ty(cimagl(M)-build_radius));
			cairo_line_to(cr,tx(creal(M)-build_radius),ty(cimagl(M)+build_radius));
		}
	}
	cairo_fill(cr);
}

static void draw_build(cairo_t *cr, struct build B){
	point M = B.X;
	cairo_set_source_rgb(cr,0,0,0);
	cairo_rectangle(cr,tx(creall(M)-build_radius),ty(cimagl(M)-build_radius),2*build_radius*zoom,2*build_radius*zoom);
	cairo_fill (cr);
}
#ifdef __MAPER__
static void draw_mbuilsID(cairo_t *cr){
	printf("draw_mbuildsID(cr);\n");
	char start;
	char finding[50];
	for(size_t i=0;i<mbuildsID_length;i++){

		start = 1;
		sprintf(finding,"/osm/way[@id = \"%d\"]/nd/@ref",mbuildsID[i]);
		printf("R\n");	
		xmlXPathObjectPtr mbuilds = getElement(finding);
		
		xmlNodeSetPtr kk = mbuilds->nodesetval;
		printf("K\n");
		size_t length = kk->nodeNr;
		int ref;
		xmlNodePtr p = *(kk->nodeTab);
		for(size_t k=0;k<length;k++){
			sscanf((char*)xmlXPathCastNodeToString(p),"%d",&ref);
			printf("ID: %d\n",ref);	
			struct geo_point sref = (struct geo_point){ref,0,0};

			struct geo_point w = *(struct geo_point*)bsearch(
				(void *)&sref,
				mbuildsID,
				mbuildsID_length,
				sizeof(struct geo_point),
				comparID); 

			if(start){
				start=0;
				cairo_move_to(cr,tx(w.lat - minlat), ty(w.lon - minlon));
			}else{
				cairo_line_to(cr,tx(w.lat - minlat), ty(w.lon - minlon));
			}
			p = p->next;
		}
		xmlFree(mbuilds);
		cairo_fill(cr);
	}
}
#endif
static gboolean repaint(gpointer widget){
//	if(thread_lock){
	el_build *elt;
	el_car *e_car;

	clear_surface();
	
	cairo_t *cr = cairo_create (surface);

	if(select_build!=NULL){
		cairo_set_source_rgb(cr,0,1,1);
		double 	x = creall(select_build->X),
				y = cimagl(select_build->X);
		cairo_arc(cr,tx(x),ty(y),build_radius+10*zoom,0,2*G_PI);
		cairo_fill(cr);
	}

	cairo_set_source_rgb(cr,1,1,0.6);
	if(see_road)
		DL_FOREACH(builds,elt){
			if(elt->b.next != NULL)
				draw_road(cr,elt->b.X,elt->b.next->X);
		}
	
	DL_FOREACH(builds,elt)
		draw_build(cr,elt->b);
	
	//for(int i=0;i<n;i++)
	DL_FOREACH(cars,e_car){
		cairo_set_source_rgb(cr,0,0,0);
		draw_car(cr,e_car->b);
		if(see_target){
			cairo_set_source_rgb(cr,0,1,0.6);
			if(e_car->b.target != NULL)
				draw_road(cr,e_car->b.X,e_car->b.target->X);
		}
	}
	cairo_destroy (cr);

#ifdef __MAPER__
	draw_mbuilsID(cr);
#endif
	gtk_widget_queue_draw(widget);
	
//	}
	
	return TRUE;
}

gboolean checkbutton_road_see(GtkBuilder *ch, gpointer usr){
	g_object_get(ch,"active",&see_road,NULL);
	return TRUE;
}
gboolean checkbutton_target_see(GtkBuilder *ch, gpointer usr){
	g_object_get(ch,"active",&see_target,NULL);
	return TRUE;
}
gboolean checkbutton_symulation_run(GtkBuilder *ch, gpointer usr){
	g_object_get(ch,"active",&run_symulation,NULL);
	return TRUE;
}
gboolean combox_mouse_action(GtkBuilder *ch, gpointer usr){
	gchar * name;
	g_object_get(ch,"active-id",&name,NULL);
	if(!strcmp(name,"build_add")){
		mouse_action_id = BUILD_ADD_ID;
	}else if(!strcmp(name,"build_rem")){
		mouse_action_id = BUILD_REM_ID;
	}else if(!strcmp(name,"car_add")){
		mouse_action_id = CAR_ADD_ID;
	}else if(!strcmp(name,"car_rem")){
		mouse_action_id = CAR_REM_ID;
	}else if(!strcmp(name,"build_target")){
		mouse_action_id = BUILD_TARGET;
	}else{
		g_printerr("Error: Unknow mouse id \"%s\" - probable error in file drawing.ui\n",name);
	}
	return TRUE;
}
/* Draw a rectangle on the surface at the given position */
/*static void draw_brush (GtkWidget *widget, gdouble x, gdouble y){
	cairo_t *cr;
	
	cr = cairo_create (surface);

	cairo_set_line_width(cr,2);

	
	draw_car(cr, cexp(3*G_PI/4*I), CMPLXL(x,y));

	cairo_destroy (cr);
	
	gtk_widget_queue_draw_area (widget, x-15, y-15, 30, 30);

}*/

/* Handle button press events by either drawing a rectangle
 * or clearing the surface, depending on which button was pressed.
 * The ::button-press signal handler receives a GdkEventButton
 * struct which contains this information.
 */
static gboolean button_press_event_cb (GtkWidget *widget, GdkEventButton *event, gpointer data){
	/* paranoia check, in case we haven't gotten a configure event */
	if (surface == NULL)
		return FALSE;

//	printf("button_press_event_cb\n");
	if(event->button == GDK_BUTTON_PRIMARY){
		point A = ctx(event->x)+cty(event->y)*I;
		struct build b;
		struct car c;
		el_car * elc;
		el_build * elb, * web;
		switch(mouse_action_id){
		case BUILD_ADD_ID:
			b = (struct build){.X = A,.id=0,.next = NULL};
			addnewbuild(b);
		break;
		case CAR_ADD_ID:
			c.X = A;
			addnewcar(c);
		break;
		case BUILD_REM_ID:
			DL_FOREACH(builds,elb){
				if(distance(A,elb->b.X)<build_radius){
					DL_FOREACH(builds,web){
						if(web->b.next == &(elb->b)){
							if(elb->b.next == &(elb->b))
								web->b.next = NULL;
							else
								web->b.next = elb->b.next;
						}

					}
					DL_FOREACH(cars,elc){
						if(elc->b.target == &(elb->b)){
							if(elb->b.next == &elb->b)
								elc->b.target = NULL;
							else
								elc->b.target = elb->b.next;
						}
					}
					if(select_build == &elb->b){
						select_build = NULL;
					}
					DL_DELETE(builds,elb);
					free(elb);
					break;
				}
			}
		break;
		case CAR_REM_ID:
			DL_FOREACH(cars,elc){
				if(distance(A,elc->b.X)<car_size+3){
					DL_DELETE(cars,elc);
					free(elc);
					break;
				}
			}

		break;
		case BUILD_TARGET:
			DL_FOREACH(builds,elb){
				if(distance(A,elb->b.X)<build_radius){
					if(select_build == NULL){
						select_build = &elb->b;
					}else{
						elb->b.next = select_build;
						select_build = NULL;
					}
					break;
				}
			}
		break;
		}
	}
	else if (event->button == GDK_BUTTON_SECONDARY){
//		clear_surface ();
//		gtk_widget_queue_draw (widget);

		pressed = TRUE;
		pressed_x = event->x;
		pressed_y = event->y;
	}
	/* We've handled the event, stop processing */
	return TRUE;
}
static gboolean button_release_event_cb (GtkWidget *widget, GdkEventButton *event, gpointer data){
//	printf("button_release_event_cb\n");

	/* paranoia check, in case we haven't gotten a configure event */
	if (surface == NULL)
		return FALSE;

/*	if (event->button == GDK_BUTTON_PRIMARY)
	{
		//draw_brush (widget, event->x, event->y);
		
	}
	else */
	if (event->button == GDK_BUTTON_SECONDARY)
	{
//		clear_surface ();
//		gtk_widget_queue_draw (widget);
		pressed = FALSE;
		translation_xd = 0;
		translation_yd = 0;
		translation_x += event->x - pressed_x;
		translation_y += event->y - pressed_y;
	}

	/* We've handled the event, stop processing */
	return TRUE;
}

/* Handle motion events by continu	ing to draw if button 1 is
 * still held down. The ::motion-notify signal handler receives
 * a GdkEventMotion struct which contains this information.
 */
static gboolean motion_notify_event_cb (GtkWidget *widget, GdkEventMotion *event, gpointer data){

	/* paranoia check, in case we haven't gotten a configure event */
	if (surface == NULL)
		return FALSE;

	if(pressed){
		translation_xd = event->x - pressed_x;
		translation_yd = event->y - pressed_y;
		
	//	printf("motion_notify_event_cb %d %d\n",translation_xd,translation_yd);
	}
/*
	if (event->state & GDK_BUTTON1_MASK)
		draw_brush (widget, event->x, event->y);
*/
	/* We've handled it, stop processing */
	return TRUE;
}

static void close_window (void){
	printf("close_window\n");
	if (surface)
		cairo_surface_destroy (surface);
}
static void activate (GtkApplication *app, gpointer user_data){
	printf("activate\n");
	
	window = gtk_application_window_new (app);
	GError *error = NULL;
	builder = gtk_builder_new ();
	GtkButton *button;
	GtkCheckButton *checkbutton;
	GtkComboBox * combox;
	
	if (gtk_builder_add_from_file (builder, "drawing.ui", &error) == 0)
	{
		g_printerr ("Error loading file: %s\n", error->message);
		g_clear_error (&error);
		exit(1);
		return;
	}
	
	GtkCssProvider *provider = gtk_css_provider_new ();
	
	if (gtk_css_provider_load_from_path (provider,"drawing.css", &error) == 0)
	{
		g_printerr ("Error loading file: %s\n", error->message);
		g_clear_error (&error);
		exit(1);
		return;
	}


	g_signal_connect (window, "destroy", G_CALLBACK (close_window), NULL);

	gtk_container_set_border_width (GTK_CONTAINER (window), 8);
	
	//frame = gtk_frame_new (NULL);
	frame = GTK_WIDGET(gtk_builder_get_object (builder, "frame"));
	
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (window), frame);

	//drawing_area = gtk_drawing_area_new ();
	drawing_area = GTK_WIDGET(gtk_builder_get_object (builder, "drawing_area"));
	/* set a minimum size */
	gtk_widget_set_size_request (drawing_area, 800, 400);

	//gtk_container_add (GTK_CONTAINER (frame), drawing_area);

	/* Signals used to handle the backing surface */
	g_signal_connect (drawing_area, "draw",
			G_CALLBACK (draw_cb), NULL);
	g_signal_connect (drawing_area,"configure-event",
			G_CALLBACK (configure_event_cb), NULL);

	/* Event signals */
	g_signal_connect (drawing_area, "motion-notify-event",
			G_CALLBACK (motion_notify_event_cb), NULL);
	g_signal_connect (drawing_area, "button-press-event",
			G_CALLBACK (button_press_event_cb), NULL);
	g_signal_connect (drawing_area, "button-release-event",
			G_CALLBACK (button_release_event_cb), NULL);

	/* Ask to receive events the drawing area doesn't normally
	 * subscribe to. In particular, we need to ask for the
	 * button press and motion notify events that want to handle.
	 */
	gtk_widget_set_events (drawing_area, gtk_widget_get_events (drawing_area)
			| GDK_BUTTON_PRESS_MASK
			| GDK_BUTTON_RELEASE_MASK
			| GDK_POINTER_MOTION_MASK);

	GtkStyleContext *context;
	context = gtk_widget_get_style_context(frame);
	gtk_style_context_add_provider (context,
                                    GTK_STYLE_PROVIDER(provider),
                                    GTK_STYLE_PROVIDER_PRIORITY_USER);
	
	button = GTK_BUTTON(gtk_builder_get_object (builder, "zoom+"));
	g_signal_connect(button,"button-press-event",G_CALLBACK(zoom_plus),NULL);
	
	button = GTK_BUTTON(gtk_builder_get_object (builder, "zoom-"));
	g_signal_connect(button,"button-press-event",G_CALLBACK(zoom_minus),NULL);
	
	button = GTK_BUTTON(gtk_builder_get_object (builder, "zoom0"));
	g_signal_connect(button,"button-press-event",G_CALLBACK(zoom_zero),NULL);
	
	button = GTK_BUTTON(gtk_builder_get_object (builder, "file_open"));
	g_signal_connect(button,"button-press-event",G_CALLBACK(file_open),NULL);

	button = GTK_BUTTON(gtk_builder_get_object (builder, "file_save"));
	g_signal_connect(button,"button-press-event",G_CALLBACK(file_save),NULL);

	checkbutton = GTK_CHECK_BUTTON(gtk_builder_get_object (builder,"see_road"));
	g_signal_connect(checkbutton,"toggled",G_CALLBACK(checkbutton_road_see),NULL);

	checkbutton = GTK_CHECK_BUTTON(gtk_builder_get_object (builder,"see_target"));
	g_signal_connect(checkbutton,"toggled",G_CALLBACK(checkbutton_target_see),NULL);

	checkbutton = GTK_CHECK_BUTTON(gtk_builder_get_object (builder,"run"));
	g_signal_connect(checkbutton,"toggled",G_CALLBACK(checkbutton_symulation_run),NULL);

	combox = GTK_COMBO_BOX(gtk_builder_get_object(builder,"mouse_action"));
	g_signal_connect(combox,"changed",G_CALLBACK(combox_mouse_action),NULL);

	gtk_widget_show_all (window);
	
	readFile(filename);

	g_timeout_add(5,repaint,drawing_area);
	g_timeout_add(3,euler,NULL);
	
	printf("END - activate\n");
}

int main (int argc,	char *argv[]){
#ifdef __MAPER__
	printf("Riding Maps\n");
	Mapermain();
	printf("Ridings end maping\n");
#endif
	GtkApplication *app;
	int status;

	if(argc > 1){
		filename = argv[1];
	}

	app = gtk_application_new ("pl.samlai.traffic", G_APPLICATION_FLAGS_NONE);
	g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
	status = g_application_run (G_APPLICATION (app), 0, NULL);
	g_object_unref (app);

	return status;
}

