#include <stdio.h>
#include "euklides.h"

/*
static point point_m(const point a){
	point w = {- a.x, - a.y};
	return w;
}*/

static struct section section_from_vector(const point P,const vector V){
	struct section w = {P, P + V};
	return w;
}

static long long int det(const double a11,const double a12,const double a21,const double a22){
	return a11*a22-a21*a12;
}

static long long int det_w(const point a,const point b ){
	return det(creal(a),cimag(a),creal(b),cimag(b));
}
double distance(point A, point B){
	return cabs(A-B);
}
/*
static long long int wiekszy(const point a,const point b){
	return creal(a)>creal(b)||(creal(a)==creal(b) && cimag(a) > cimag(b));
}*/
/*
static struct section uklad(const point a,const point b){
	struct section w;
	if(wiekszy(a,b)){
		w.a = b;
		w.b = a;
	}else{
		w.a = a;
		w.b = b;	
	}
	return w;
}*/

int przecina(const point a1,const point b1,const point a2,const point b2){
	long long int W,Wt1,Wt2;
	point 	ba1 = b1 - a1,
			ba2 = a2 - b2,
			aa  = a2 - a1;
	W = det_w(ba1,ba2);
//	printf("W=%lld\n",W);
	Wt1 = det_w(aa,ba2);
	Wt2 = det_w(ba1,aa);
	if(W==0){
		return 0;
/*		if( Wt1==0 && Wt2==0){
			struct section A,B;
			A = uklad(a1,b1);
			B = uklad(a2,b2);
			
			return !( wiekszy(B.a,A.b) || wiekszy(A.a,B.b) );
		}
		else{
			return 0;	
		}*/
	}
//	printf("Wt1=%lld Wt2=%lld\n",Wt1,Wt2);
//	double t1=Wt1*1.0/W,t2=Wt2*1.0/W;
//	printf("t1=%lf t2=%lf\n",t1,t2);
	
	return Wt1*1.0/W>=0 && Wt1*1.0/W<=1 && Wt2*1.0/W>=0 && Wt2*1.0/W<=1;
	//return Wt1*W>=0 && (Wt1-W)*W<=0 && Wt2*W>=0 && (Wt2-W)*W<=0;
}

int przecina_w(const struct section A,const struct section B){
	return przecina(A.a,A.b,B.a,B.b);
}

long long int sgn(const long long int val) {
    return ((0 < val)?1:0) - ((val < 0)?1:0);
}

int tasama_strona(point a, point b, point p, point q){
	return sgn(det_w(b - a,p - a)) * sgn(det_w(b - a,q - a)) > 0;
}

int trojkont_strona(point a, point b, point p,const struct triangle L){
	return tasama_strona(a,b,p,L.a) && tasama_strona(a,b,p,L.b) && tasama_strona(a,b,p,L.c);
}

int trojkont_zawiera(const struct triangle K, const struct triangle L){
	if(	trojkont_strona(K.a,K.b,K.c,L) && 
		trojkont_strona(K.a,K.c,K.b,L) &&
		trojkont_strona(K.b,K.c,K.a,L)){
			//printf("zawiera (%lld,%lld) (%lld,%lld)\n",K.v.x,K.v.y,L.v.x,L.v.y);
			return 1;
	}
	if(	trojkont_strona(L.a,L.b,L.c,K) && 
		trojkont_strona(L.a,L.c,L.b,K) &&
		trojkont_strona(L.b,L.c,L.a,K)){
			//printf("zawiera (%lld,%lld) (%lld,%lld)\n",L.v.x,L.v.y,K.v.x,K.v.y);
			return 1;
	}
	return 0;
}
void odcinki(struct section KA[], const struct triangle K){
	KA[0].a=K.a;
	KA[0].b=K.b;

	KA[1].a=K.a;
	KA[1].b=K.c;

	KA[2].a=K.b;
	KA[2].b=K.c;
}
int trojkont_przecina(const struct triangle K, const struct triangle L){
	struct section KA[3],LA[3];
	odcinki(KA,K);
	odcinki(LA,L);
	
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			if(przecina_w(KA[i],LA[j])){
					return 1;
			}
		}
	}
	return 0;
}

int trojkont_CzescWspulna(const struct triangle K, const struct triangle L){
	return trojkont_przecina(K,L) || trojkont_zawiera(K,L);
}

int przetnie_w_przyszlosci(const struct triangle K, point Kv, const struct triangle L, point Lv){
	
	struct section KA[3],LA[3];
	odcinki(KA,K);
	odcinki(LA,L);
	
	point V = Kv - Lv;
	for(int i=0;i<3;i++){
		if(przecina_w(section_from_vector(K.a,V),LA[i])){
			return 1;
		}
		if(przecina_w(section_from_vector(K.b,V),LA[i])){
			return 1;
		}
		if(przecina_w(section_from_vector(K.c,V),LA[i])){
			return 1;
		}
	}
	
	V = Lv - Kv;
	for(int i=0;i<3;i++){
		if(przecina_w(section_from_vector(L.a,V),KA[i])){
			return 1;
		}
		if(przecina_w(section_from_vector(L.b,V),KA[i])){
			return 1;
		}
		if(przecina_w(section_from_vector(L.c,V),KA[i])){
			return 1;
		}
	}
	
	return 0;
}

