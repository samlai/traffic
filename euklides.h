#include <complex.h>
#include <math.h>

typedef double complex point;

#define vector point

#ifndef __section__
#define __section__
struct section{
	point a,b;	
};

struct triangle{
	point a,b,c;
};
#endif

int trojkont_przecina(const struct triangle K, const struct triangle L);

int przetnie_w_przyszlosci(const struct triangle K, vector Kv, const struct triangle L, vector Lv);

double distance(point,point);
